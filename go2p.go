package main

import (
	"fmt"
	"go2p/sdsai/app"
	"go2p/sdsai/p2p"
	"go2p/ui"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	// Build an app, a p2p object and start it.
	app := app.NewApp("go2p", "1.0")

	// Put command line arguments into the mix.
	mergeArgs(app)

	p2p := p2p.NewP2p(app)
	err := p2p.Startup()
	if err != nil {
		app.Log.Fatal(err)
	}

	ui, err := ui.NewUi(app, p2p)
	ui.Startup()

	// Start service.

	// Boot strap into the network.
	p2p.AddPeer("seed", true, "localhost:8764")
	p2p.AddConn("seed")

	// Shutdown code.
	sigint := make(chan os.Signal)
	signal.Notify(sigint, syscall.SIGINT)
	<-sigint
	app.Log.Printf("Shutting down.")
	ui.Shutdown()
	p2p.Shutdown()
}

// Merge command line arguments into an app.Config held by an *App.
func mergeArgs(app *app.App) {
	args := os.Args[1:]

	for i := 0; i < len(args); i++ {
		if i+1 < len(args) {
			if args[i] == "--bind" || args[i] == "-b" {
				i++
				app.Config.SetStr("p2p.bind", args[i])
			} else if args[i] == "--port" || args[i] == "-p" {
				i++
				app.Config.SetStr("p2p.port", args[i])
			}
		}

		if args[i] == "--help" || args[i] == "-h" {
			fmt.Printf(`%s [options]
--port | -p Port to bind.
--bind | -b IP address to bind.
`, os.Args[0])
			os.Exit(0)
		}
	}
}
