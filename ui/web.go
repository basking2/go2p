package ui

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"go2p/sdsai/app"
	"go2p/sdsai/p2p"
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

type Ui struct {
	app         *app.App
	log         *log.Logger
	p2p         *p2p.P2p
	listener    net.Listener
	tlslistener net.Listener
	port        string
	bind        string
	Certs       string
	Keys        string
}

func (ui *Ui) Startup() error {

	mux := http.NewServeMux()
	ui.buildRoutes(mux)
	if err := ui.listenAndServe(ui.app, ui.p2p, mux); err != nil {
		ui.log.Fatal(err)
		return err
	}

	return nil
}

func (ui *Ui) Shutdown() {
	ui.listener.Close()
	ui.tlslistener.Close()
}

func NewUi(app *app.App, p2p *p2p.P2p) (*Ui, error) {
	config := app.Config

	ui := new(Ui)
	ui.p2p = p2p
	ui.app = app
	ui.log = app.Log
	ui.port = config.GetStr("ui.port", "8443")
	ui.bind = config.GetStr("ui.bind", "::")

	return ui, nil
}

func (ui *Ui) listenAndServe(app *app.App, p2p *p2p.P2p, handler http.Handler) error {
	var err error
	var config = app.Config
	var addr = net.JoinHostPort(
		config.GetStr("ui.bind", "::"),
		config.GetStr("ui.port", "8080"))
	var tlsaddr = net.JoinHostPort(
		config.GetStr("ui.tls.bind", "::"),
		config.GetStr("ui.tls.port", "8443"))
	var certs string
	var keys string
	if certs, err = config.GetFile("ui.certs.file", "cert.pem"); err != nil {
		return err
	}
	if keys, err = config.GetFile("ui.keys.file", "privkey.pem"); err != nil {
		return err
	}

	// TLS port.
	tlsconfig := new(tls.Config)
	tlsconfig.ClientAuth = tls.NoClientCert
	tlsconfig.RootCAs = x509.NewCertPool()
	tlsconfig.ClientCAs = x509.NewCertPool()
	if pubpem, err := ioutil.ReadFile(certs); err == nil {
		tlsconfig.RootCAs.AppendCertsFromPEM(pubpem)
		tlsconfig.ClientCAs.AppendCertsFromPEM(pubpem)
	} else {
		return errors.New(fmt.Sprintf("certs file %s: %s", certs, err))
	}

	if privpem, err := ioutil.ReadFile(keys); err == nil {
		tlsconfig.RootCAs.AppendCertsFromPEM(privpem)
		tlsconfig.ClientCAs.AppendCertsFromPEM(privpem)
	} else {
		return errors.New(fmt.Sprintf("keys file %s: %s", keys, err))
	}

	if crt, err := tls.LoadX509KeyPair(certs, keys); err == nil {
		tlsconfig.Certificates = make([]tls.Certificate, 1)
		tlsconfig.Certificates[0] = crt
	} else {
		return err
	}
	if lst, err := tls.Listen("tcp", tlsaddr, tlsconfig); err == nil {
		ui.log.Printf("Listening on %s.", tlsaddr)
		ui.tlslistener = lst
	} else {
		return err
	}

	// Normal port.
	if lst, err := net.Listen("tcp", addr); err == nil {
		ui.log.Printf("Listening on %s.", addr)
		ui.listener = lst
	} else {
		return err
	}

	// These must go into goroutines.
	go http.Serve(ui.listener, handler)
	go http.Serve(ui.tlslistener, handler)

	return nil
}

// Build the web routes and attach handlers.
func (ui *Ui) buildRoutes(mux *http.ServeMux) {
	mux.HandleFunc(
		"/ping",
		func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			resp := make(map[string]string)
			resp["response"] = "pong"
			json.NewEncoder(w).Encode(resp)
		})
	mux.Handle("/txt/", NewTxtService(ui.p2p))
	mux.Handle("/txt/", NewMgmtService(ui.p2p))
}
