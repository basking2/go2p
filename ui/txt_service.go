// Contains simple text service for p2p.

package ui

import (
	"encoding/json"
	"fmt"
	"go2p/sdsai/p2p"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"
)

var SERVICE_NAME = "txt"

type TxtService struct {
	p2p *p2p.P2p

	// Shared state. Must be locked.
	messages map[string]string

	// Lock for messages.
	messagesLock *sync.Mutex
}

// Build a service.
func NewTxtService(p2p *p2p.P2p) *TxtService {
	txtsvc := new(TxtService)
	txtsvc.p2p = p2p
	txtsvc.messages = make(map[string]string)
	txtsvc.messagesLock = new(sync.Mutex)

	p2p.RegisterHandler(SERVICE_NAME, txtsvc)

	return txtsvc
}

// Handle a p2p message.
func (ts *TxtService) HandleP2p(p2p *p2p.P2p, msg *p2p.P2pMsg) error {
	id := fmt.Sprintf("%s-%s", time.Now(), msg.Header.From)

	ts.messagesLock.Lock()
	ts.messages[id] = string(msg.Data)
	ts.messagesLock.Unlock()

	return nil
}

// Regexps for parsing URLs etc.
var msgRegexp = regexp.MustCompile("/message/(.*)")
var peerRegexp = regexp.MustCompile("/peer/id/(.*)/addr/(.*)")

// Handle user requests to the service.
func (ts *TxtService) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	path := req.URL.Path

	if strings.LastIndex(path, "/messages/all") == len(path)-13 {
		w.Write([]byte("All Messages\n"))
		ts.messagesLock.Lock()
		for msgId, msgBody := range(ts.messages) {
			w.Write([]byte(msgId))
			w.Write([]byte("\t"))
			w.Write([]byte(msgBody))
		}
		ts.messagesLock.Unlock()
	} else if strs := msgRegexp.FindStringSubmatch(path); strs != nil {
		msgid := strs[1]

		switch req.Method {
		case "GET":
			ts.messagesLock.Lock()
			msg, found := ts.messages[msgid]
			ts.messagesLock.Unlock()

			if found {
				w.WriteHeader(http.StatusNotFound)
				json.NewEncoder(w).Encode(msg)
			} else {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("Message "))
				w.Write([]byte(strs[1]))
				w.Write([]byte(" not found."))
			}
		case "POST", "PUT":
			msgid := p2p.PeerId(msgid)
			msg, err := ioutil.ReadAll(req.Body)
			if err != nil {
				ts.p2p.Send(msgid, 3, SERVICE_NAME, msg)
				w.WriteHeader(http.StatusNoContent)
			} else {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte(err.Error()))
			}
		}
	} else if strs := peerRegexp.FindStringSubmatch(path); strs != nil {
		var connect bool = true
		var peerId = strs[1]
		var addr = strs[2]

		switch req.FormValue("connect") {
		case "no", "false", "0":
			connect = false
		default:
			connect = true
		}

		ts.p2p.AddPeer(peerId, true, addr)
		if connect {
			ts.p2p.AddConn(peerId)
		}

		w.WriteHeader(http.StatusNoContent)
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not found."))
	}
}
