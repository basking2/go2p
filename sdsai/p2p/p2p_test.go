package p2p

import (
	"go2p/sdsai/app"
	"testing"
)

func TestP2p(t *testing.T) {
	config := new(app.Config)
	p2p := NewP2p(config)
	p2p.Shutdown()
}

func TestP2pAddPeer(t *testing.T) {
	config := new(app.Config)
	p2p := NewP2p(config)
	p2p.AddPeer("bob", false, "localhost:18003")
	p2p.Shutdown()
}

// Make shutdown succeed when a connection fails.
func TestP2pAddConnFails(t *testing.T) {
	config := new(app.Config)
	p2p := NewP2p(config)
	p2p.AddPeer("bob", false, "localhost:18003")
	p2p.AddConn("bob")
	p2p.Shutdown()
}
