package p2p

import (
	"fmt"
	"go2p/sdsai/app"
	"log"
	"math/rand"
	"net"
	"sort"
	"time"
)

type P2pHandler interface {
	HandleP2p(*P2p, *P2pMsg) error
}

type P2p struct {
	id     PeerId
	log    *log.Logger
	config *app.Config
	peers  map[PeerId]*Peer
	conns  map[PeerId]*Conn
	// Services.
	handlers map[string]P2pHandler
	ctrl     chan interface{}

	// A semaphore for the recieve channel.
	recieveSemaphoreChan chan int

	port string
	bind string

	Certs string
	Keys  string
}

// A connection to a Peer.
type Conn struct {
	Id       PeerId
	LastUsed time.Time
	Conn     net.Conn
	Io       chan interface{}
}

type PeerId string

// Another computer we can talk to.
type Peer struct {
	// The unique ID of this peer.
	Id PeerId

	// Did we connect to this (was the connection outbound)?
	Outbound bool

	// address
	Addr string

	// Last seen.
	LastSeen time.Time

	// Last connection we saw this host on.
	// This is the remote IP and port of the connected host as a string.
	LastConn PeerId
}

func NewP2p(app *app.App) *P2p {
	config := app.Config
	p2p := new(P2p)

	p2p.id = PeerId(
		config.GetStr("p2p.id", fmt.Sprintf("%d", rand.Uint32())))
	p2p.port = config.GetStr("p2p.port", "8654")
	p2p.bind = config.GetStr("p2p.bind", "::")
	p2p.peers = make(map[PeerId]*Peer)
	p2p.conns = make(map[PeerId]*Conn)
	p2p.handlers = make(map[string]P2pHandler)
	p2p.ctrl = make(chan interface{})
	p2p.config = config
	p2p.log = app.Log

	// Build the receive message semaphore.
	p2p.recieveSemaphoreChan = make(chan int, 1000)
	for i := 0; i < 1000; i++ {
		p2p.recieveSemaphoreChan <- 1
	}

	// Get SSL cert values.
	if certs, err := config.GetFile("p2p.certs.file", "cert.pem"); err == nil {
		p2p.Certs = certs
	}

	// Get SSL key values.
	if keys, err := config.GetFile("p2p.keys.file", "privkey.pem"); err == nil {
		p2p.Keys = keys
	}

	go p2pMsgHandler(p2p)

	return p2p
}

func (p2p *P2p) Startup() error {
	addr := net.JoinHostPort(p2p.bind, string(p2p.port))
	ltr, err := p2p.listener(addr)
	if err != nil {
		return err
	}

	go func() {
		for err = p2p.accept(ltr); err == nil; {
			// nop
		}
	}()

	return nil
}

func (p2p *P2p) Shutdown() {
	p2p.ctrl <- MsgShutdown{}
}

/*
 Add / replace a peer record.
 If id is empty a random one will generated.
*/
func (p2p *P2p) AddPeer(id string, outbound bool, addr string) {
	if id == "" {
		id = fmt.Sprintf("%d", rand.Uint32())
	}

	p := &Peer{
		Id:       PeerId(id),
		Outbound: outbound,
		Addr:     addr,
		LastSeen: time.Now(),
		LastConn: PeerId(""),
	}

	p2p.ctrl <- MsgAddPeer{p}
}

// Connect to a peer.
func (p2p *P2p) AddConn(peer string) {
	p2p.ctrl <- MsgOpen{id: PeerId(peer)}
}

// Add a peer and a live connection.
func (p2p *P2p) AddPeerConn(peer string, conn net.Conn) {
	p2p.ctrl <- MsgPeerConn{id: PeerId(peer), conn: conn}
}

// Remove a peer and its connection, if that exists.
func (p2p *P2p) RemoveConn(connId string) {
	p2p.ctrl <- MsgClose{PeerId(connId)}
}

// Remove a peer and its connection, if that exists.
func (p2p *P2p) RemovePeer(peerId string) {
	p2p.ctrl <- MsgRemovePeer{PeerId(peerId)}
}

/*
Wrap the user's data into a P2pMsg and send it via SendMsg().
*/
func (p2p *P2p) Send(to PeerId, hops uint8, app string, data []byte) {
	m := P2pMsg{
		Header: P2pMsgHeader{
			To:   to,
			From: p2p.id,
			Hops: hops,
			App:  app,
			Path: []PeerId{p2p.id},
		},
		Data: data,
	}

	p2p.SendMsg(m)
}

func (p2p *P2p) SendMsg(msg P2pMsg) {
	p2p.ctrl <- MsgSend{&msg, p2p.id}
}

func (p2p *P2p) FwdMsg(msg P2pMsg, receivedFrom PeerId) {
	var path = make([]PeerId, len(msg.Header.Path)+1)
	copy(path, msg.Header.Path)

	// Do not forward something for which we are already in the path.
	for _, id := range(path) {
		if id == p2p.id {
			return
		}
	}

	// Add ourselves to the end of the path.
	path[len(msg.Header.Path)] = p2p.id

	// Make a new message object so we don't mutate it's path.
	// This, in practice, is probably uncessary, but it is safe and correct.
	var newMsg = P2pMsg{ msg.Header, msg.Data }
	newMsg.Header.Path = msg.Header.Path

	p2p.ctrl <- MsgSend{&msg, receivedFrom}
}

/*
 A sortable type of peer array that sorts from oldest seen to latest.
*/
type sortablePeerList []*Peer

func (pl sortablePeerList) Len() int { return len(pl) }
func (pl sortablePeerList) Less(i, j int) bool {
	lastSeeni, lastSeenj := pl[i].LastSeen, pl[j].LastSeen
	return lastSeeni.Before(lastSeenj)
}
func (pl sortablePeerList) Swap(i, j int) {
	pl[i], pl[j] = pl[j], pl[i]
}

// Limit the current number of known peers.
// A peer is not removed if it has an open connection.
//
// This is not thread safe. It should be moved to the handler goroutine.
//
func (p2p *P2p) LimitPeers(max int) {
	toRemove := len(p2p.peers) - max
	if toRemove <= 0 {
		return
	}

	peerList := sortablePeerList(make([]*Peer, len(p2p.peers)))
	i := 0
	for _, p := range p2p.peers {
		peerList[i] = p
		i += 1
	}

	// Sort peers so the oldest are at the start of the list.
	sort.Sort(peerList)

	i = 0
	for i = 0; i < toRemove; i++ {
		p2p.RemovePeer(string(peerList[i].Id))
	}
}

// Register a handler for an app.
// This is not thread-safe and should only be done before Startup() is called.
func (p2p *P2p) RegisterHandler(app string, handler P2pHandler) {
	p2p.handlers[app] = handler
}
