package p2p

import (
	"crypto/tls"
	"go2p/sdsai/app"
	"net"
	"testing"
)

func TestP2pConn(t *testing.T) {
	p2p := NewP2p(new(app.Config))

	// Set the cert files.
	p2p.Certs = "cert.pem"
	p2p.Keys = "privkey.pem"

	l, e := p2p.listener("localhost:8489")
	if e != nil {
		panic(e)
	}

	go func(l net.Listener) {
		conn, err := l.Accept()

		if err != nil {
			panic(err)
		}

		if _, err := readP2pMsg(conn); err != nil {
			panic(err)
		}

		l.Close()

	}(l)

	p := Peer{Id: "test", Addr: "localhost:8489"}
	c, err := p2p.connect(&p)
	if err != nil {
		panic(err)
	}

	c.Close()
}

func TestClientAuth(t *testing.T) {
	config := new(app.Config)
	p2p := NewP2p(config)

	// Set the cert files.
	p2p.Certs = "cert.pem"
	p2p.Keys = "privkey.pem"

	l, e := p2p.listener("localhost:8489")
	if e != nil {
		panic(e)
	}

	go func(l net.Listener) {
		conn, err := l.Accept()
		if err != nil {
			panic(err)
		}

		msg := make([]byte, 4)

		// Reading forces an SSL negotiation.
		if _, err := conn.Read(msg); err == nil {
			panic("Certificate was expected to be rejected!")
		}

		l.Close()
	}(l)

	_, err := tls.Dial("tcp", "localhost:8489", &tls.Config{
		InsecureSkipVerify: true,
	})
	if err == nil {
		panic("Our connection should have been rejected!")
	}

}
