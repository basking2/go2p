package p2p

/*
 How P2p makes connections.
*/

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
)

// Build a tls.Config for peer-to-peer connections.
// Mutual authentication but no host name verification.
func (p2p *P2p) tlsConfig() (*tls.Config, error) {
	certs := p2p.Certs
	keys := p2p.Keys

	c := new(tls.Config)
	c.ClientAuth = tls.RequireAndVerifyClientCert
	//c.ClientAuth = tls.NoClientCert
	c.RootCAs = x509.NewCertPool()
	c.ClientCAs = x509.NewCertPool()
	c.InsecureSkipVerify = true

	if pubpem, err := ioutil.ReadFile(certs); err == nil {
		c.RootCAs.AppendCertsFromPEM(pubpem)
		c.ClientCAs.AppendCertsFromPEM(pubpem)
	} else {
		return nil, errors.New(fmt.Sprintf("certs file %s: %s", certs, err))
	}

	if privpem, err := ioutil.ReadFile(keys); err == nil {
		c.RootCAs.AppendCertsFromPEM(privpem)
		c.ClientCAs.AppendCertsFromPEM(privpem)
	} else {
		return nil, errors.New(fmt.Sprintf("keys file %s: %s", keys, err))
	}

	if crt, err := tls.LoadX509KeyPair(certs, keys); err == nil {
		c.Certificates = make([]tls.Certificate, 1)
		c.Certificates[0] = crt
	} else {
		return nil, err
	}

	return c, nil
}

// Establish a connection and handshake.
func (p2p *P2p) connect(p *Peer) (net.Conn, error) {
	config, err := p2p.tlsConfig()
	if err != nil {
		return nil, err
	}

	conn, err := tls.Dial("tcp", p.Addr, config)
	if err != nil {
		return nil, err
	}

	// If we do node authentication, this is where it should go.
	msg := P2pMsg{
		Header: P2pMsgHeader{
			To:   p2p.id,
			From: p2p.id,
			Hops: 0,
		},
		Data: nil,
	}

	msg.writeP2pMsg(conn)

	return conn, err
}

/*
 Listen for connections.
*/
func (p2p *P2p) listener(addr string) (net.Listener, error) {

	config, err := p2p.tlsConfig()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("tls config: %s", err))
	}

	lst, err := tls.Listen("tcp", addr, config)
	if err != nil {
		return nil, err
	}

	return lst, nil
}

// Accept a connection and handshake in a goroutine.
func (p2p *P2p) accept(l net.Listener) error {
	conn, err := l.Accept()
	if err != nil {
		return err
	}

	// handshake
	go func(conn net.Conn) {
		msg, err := readP2pMsg(conn)
		if err != nil {
			return
		}

		// Register this peer with this connection.
		p2p.ctrl <- MsgPeerConn{msg.Header.From, conn}

		// a normal message!
		if msg.Header.From == p2p.id {
			// nop - from ourselves?
		} else if msg.Header.To == p2p.id {
			// nop - to ourselves?
		} else if len(msg.Data) > 0 && msg.Header.Hops > 0 {
			// If the first message has data, let's treat it like
			// Reduce the lifetime of the message
			msg.Header.Hops -= 1
			p2p.ctrl <- MsgSend{msg, msg.Header.From}
		}
	}(conn)

	return nil
}
