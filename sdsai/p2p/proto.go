package p2p

import (
	"bytes"
	"encoding/json"
	"io"
)

/*
The message wire protocol.
*/

// The header of a P2p message.
type P2pMsgHeader struct {
	// To which peer is this being sent. Empty for none.
	To PeerId
	// Which peer sent this. Empty for none.
	From PeerId
	// How many retransmissions would we like.
	Hops uint8
	// The application on the other end we are sending to.
	App string
	// The node id hops that have already been taken. Used to prune.
	Path []PeerId
}

/*
 A user message for the p2p system.
*/
type P2pMsg struct {
	Header P2pMsgHeader
	Data   []byte
}

func writeUint32(w io.Writer, i uint32) {
	l := make([]byte, 4)

	l[0] = byte(i>>24) & 0xff
	l[1] = byte(i>>16) & 0xff
	l[2] = byte(i>>8) & 0xff
	l[3] = byte(i) & 0xff

	w.Write(l)
}

func readUint32(r io.Reader) (uint32, error) {
	msg := make([]byte, 4)
	_, err := io.ReadFull(r, msg)

	l := uint32(0)
	l = l | uint32(msg[0]<<24)
	l = l | uint32(msg[1]<<16)
	l = l | uint32(msg[2]<<8)
	l = l | uint32(msg[3])

	return l, err
}

/*
 Write |header len | data len | header | data |
*/
func (m *P2pMsg) writeP2pMsg(w io.Writer) {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.Encode(m.Header)
	msg := buf.Bytes()

	writeUint32(w, uint32(len(msg)))
	writeUint32(w, uint32(len(m.Data)))
	w.Write(msg)
	w.Write(m.Data)
}

/*
Read |header len | data len | header | data |
*/
func readP2pMsg(r io.Reader) (*P2pMsg, error) {

	// Read the header length.
	headerLen, err := readUint32(r)
	if err != nil {
		return nil, err
	}

	// Read the data length.
	dataLen, err := readUint32(r)
	if err != nil {
		return nil, err
	}

	// Read the JSON header.
	msg := make([]byte, headerLen)
	_, err = io.ReadFull(r, msg)
	if err != nil {
		return nil, err
	}

	p := new(P2pMsg)

	dec := json.NewDecoder(bytes.NewBuffer(msg))
	if err := dec.Decode(&p.Header); err != nil && err == io.EOF {
		return nil, err
	}

	// Read the data segment.
	p.Data = make([]byte, dataLen)
	_, err = io.ReadFull(r, p.Data)
	if err != nil {
		return nil, err
	}

	return p, err
}
