package p2p

import (
	"net"
	"time"
)

/*
 Messages that P2P uses to manage itself. These are not sent over the network.
*/

// Shutdown everything.
// When peer is not set, the user is requesting this.
// When peer is set a connection sent it to report it is done.
// When all connections have reported, the conns map is empty
// and the system is fully shutdown.
type MsgShutdown struct {
	id PeerId
}

// Close the channel and destroy the receiver's managed data.
type MsgClose struct {
	id PeerId
}

// Open a connection to a peer.
type MsgOpen struct {
	id PeerId
}

// Send data on the connection.
type MsgSend struct {
	msg          *P2pMsg
	receivedFrom PeerId
}

// Add peer to the p2p records.
type MsgAddPeer struct {
	peer *Peer
}

// Remove a peer from the system and close the connection to it.
// The data field is the peer id.
type MsgRemovePeer struct {
	id PeerId
}

// Add a peer with a direct connection.
type MsgPeerConn struct {
	id   PeerId
	conn net.Conn
}

// Recv data from a connection.
// Peer is set to the peer of the connection.
type MsgRecv struct {
	// The peer we received this message from. Not the `from` field in the message.
	peer *Peer
	// The message we've recived.
	msg *P2pMsg
}

/*
 The "server" side of the p2p system.

 This is the only code that may mutate the p2p object.
*/
func p2pMsgHandler(p2p *P2p) {
	for msg := range p2p.ctrl {
		switch msg := msg.(type) {
		case MsgShutdown:
			// If true, the user sent this.
			if string(msg.id) == "" {
				// Tell all connections to close.
				for _, c := range p2p.conns {
					c.Io <- MsgClose{}
				}
			} else {
				// A connection sent this. Remove it.
				delete(p2p.conns, msg.id)
			}

			// When all connections are closed, we're done.
			if len(p2p.conns) == 0 {
				close(p2p.ctrl)
				return
			}
		case MsgClose:
			id := msg.id
			conn, exists := p2p.conns[id]
			if exists {
				conn.Io <- msg
				delete(p2p.conns, id)
			}
		case MsgOpen:
			p, exists := p2p.peers[msg.id]
			if !exists {
				continue
			}

			if p.Addr == "" {
				continue
			}

			// Dial the peer.
			var err error
			conn, err := p2p.connect(p)
			if err != nil {
				continue
			}

			// We're connecing to p. It is the ID of conn.
			p.LastConn = p.Id
			p.LastSeen = time.Now()
			p.Outbound = true

			newConnection(p2p, p, conn)

			p2p.peers[p.Id] = p

		case MsgSend:
			p, exists := p2p.peers[msg.msg.Header.To]
			if exists {
				// Do we have a direct connection?
				c, exists := p2p.conns[p.Id]
				if exists {
					c.Io <- msg
					continue
				}

				// Do we have a last-seen connection?
				c, exists = p2p.conns[p.LastConn]
				if exists {
					c.Io <- msg
					continue
				}
			}

			// Broadcast.
			for id, c := range p2p.conns {
				if id != msg.receivedFrom {
					c.Io <- msg
				}
			}
		case MsgRecv:
			p2p.recieve(&msg)
		case MsgAddPeer:
			p := msg.peer
			p2p.peers[p.Id] = p
		case MsgRemovePeer:
			id := msg.id
			delete(p2p.peers, id)
			conn, exists := p2p.conns[id]
			if exists {
				conn.Io <- MsgClose{msg.id}
				delete(p2p.conns, id)
			}
		case MsgPeerConn:
			p, exists := p2p.peers[msg.id]

			// If p is missing, add it.
			if !exists {
				p = new(Peer)
				p.LastSeen = time.Now()
				p.LastConn = p.Id
				p.Outbound = false
				p2p.peers[p.Id] = p
			}

			// Regardless, we will set p's Addr to the remote address on the Conn.
			p.Addr = msg.conn.RemoteAddr().String()

			newConnection(p2p, p, msg.conn)

		default:
			panic(msg)
		}
	}
}

func newConnection(p2p *P2p, p *Peer, conn net.Conn) {
	c := new(Conn)
	c.Id = p.Id
	c.Conn = conn
	c.LastUsed = time.Now()
	c.Io = make(chan interface{})

	go connMsgHandler(p2p, c)
	go connMsgReceiver(p2p, p, c)

	p2p.conns[c.Id] = c
}

// Receive a message from the network.
// This is called by the p2p handler and so may edit p2p.
func (p2p *P2p) recieve(msg *MsgRecv) {
	// Mark the peer that received this message as active
	// via a direct connection.
	msg.peer.LastSeen = time.Now()
	msg.peer.LastConn = msg.peer.Id

	select {
	case i := <-p2p.recieveSemaphoreChan:
		// Send the message to the user not on the p2p ctrl goroutine.
		go func() {
			// Don't die if recv closes before we can send.
			// recv only closes on p2p shutdown, so just drop
			// the message. No harm.
			defer recover()

			// If we have a handler, dispatch to it.
			if handler, haveHandler := p2p.handlers[msg.msg.Header.App]; haveHandler {
				handler.HandleP2p(p2p, msg.msg)
				p2p.recieveSemaphoreChan <- i
			}
		}()
	default:
		// No slots left to take from our channel. Drop msg.
	}
}

// Constantly receive from this connection.
func connMsgReceiver(p2p *P2p, peer *Peer, conn *Conn) {

	for {
		p, err := readP2pMsg(conn.Conn)
		if err != nil {
			p2p.RemoveConn(string(conn.Id))
			return
		}

		p2p.ctrl <- MsgRecv{peer, p}
	}
}

/*
 The "server" side of the p2p connection system.

 This code may not mutate the p2p object, only the connections.
*/
func connMsgHandler(p2p *P2p, conn *Conn) {
	for {
		select {
		case msg := <-conn.Io:
			switch msg := msg.(type) {
			case MsgShutdown:
				conn.Conn.Close()
				close(conn.Io)
				p2p.ctrl <- MsgShutdown{conn.Id}
				return
			case MsgClose:
				conn.Conn.Close()
				close(conn.Io)
				return
			case MsgSend:
				msg.msg.writeP2pMsg(conn.Conn)
			default:
				panic(msg)
			}
		}
	}
}
