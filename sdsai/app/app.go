package app

import (
	"fmt"
	"log"
	"os"
	"path"
)

type App struct {
	Name      string
	Version   Version
	ConfigDir string
	Config    *Config
	Log       *log.Logger
}

func NewApp(name string, version Version) *App {
	app := new(App)

	app.Name = name
	app.Version = version

	logPrefix := fmt.Sprintf("%s-%s: ", name, version)

	app.Log = log.New(os.Stdout, logPrefix, log.LstdFlags|log.Lshortfile)

	app.Config = NewConfig()
	app.makeConfigDir()
	app.loadConfig()

	return app
}

func (app *App) loadConfig() {
	configFileName := path.Join(app.ConfigDir, "config")

	configFile, err := os.Open(configFileName)

	if err == nil {
		app.Config.LoadFile(configFile)
	} else {
		app.Log.Printf("Cannot open config file: %s", err)
	}
}

/*
 Create and set the config directory.
*/
func (app *App) makeConfigDir() {

	if app.ConfigDir == "" {
		var home = os.Getenv("HOME")
		if home == "" {
			home = "."
		}
		var dirname = fmt.Sprintf(".%s", app.Name)
		app.ConfigDir = path.Join(home, dirname)
	}
	e := os.MkdirAll(app.ConfigDir, 0700)

	if e == nil {
		//app.Log.Printf("Config directory: %s", app.ConfigDir)
	} else {
		app.Log.Fatal("Creating config directory: ", e)
	}
}
