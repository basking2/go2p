package app

import (
	"container/list"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"strings"
)

type Config struct {
	config map[string]string
	files  *list.List
}

func (config *Config) LoadFile(f *os.File) error {
	info, err := f.Stat()
	if err != nil {
		return err
	}

	data := make([]byte, info.Size())

	_, err = io.ReadFull(f, data)

	txt := string(data)

	config.files.PushBack(f.Name())

	return config.LoadString(txt)
}

func (config *Config) GetStr(name string, deflt string) string {
	v, notfound := config.config[name]
	if notfound {
		return v
	}

	return deflt
}

func (config *Config) GetUint16(name string, deflt uint16) uint16 {
	v, notfound := config.config[name]
	if notfound {
		return deflt
	}

	vi, err := strconv.ParseUint(v, 0, 16)
	if err == nil {
		return uint16(vi)
	}

	return deflt
}

func (config *Config) GetInt(name string, deflt int) int {
	v, notfound := config.config[name]
	if notfound {
		return deflt
	}

	vi, err := strconv.ParseInt(v, 0, 32)
	if err == nil {
		return int(vi)
	}

	return deflt
}

/*
Return path to the file of the given key relative to a given config file or an error.
*/
func (config *Config) GetFile(name string, deflt string) (string, error) {
	f := config.GetStr(name, deflt)

	if path.IsAbs(f) {
		return f, nil
	}

	return config.RelativeFile(f)
}

func NewConfig() *Config {
	c := new(Config)
	c.config = make(map[string]string)
	c.files = list.New()
	return c
}

func (config *Config) LoadString(txt string) error {
	for lineno, line := range strings.Split(txt, "\n") {

		// Skip lines with no data: comments and empty.
		if v := strings.TrimLeft(line, "\r\n\t "); len(v) == 0 {
			// Empty line.
			continue
		} else if v[0] == '#' {
			// Comment line.
			continue
		}

		toks := strings.SplitN(line, "=", 2)
		if len(toks) == 2 {
			key := strings.Trim(toks[0], "\r\n\t ")
			value := strings.Trim(toks[1], "\r\n\t ")
			config.config[key] = value
		} else {
			msg := fmt.Sprintf("Invalid configuration on line %d.", lineno+1)
			return errors.New(msg)
		}
	}

	return nil
}

func (config *Config) SetStr(key, value string) {
	config.config[key] = value
}

/*
Return a file that exists and is relative to one of the loaded config files.
*/
func (config *Config) RelativeFile(relativeFile string) (string, error) {
	if config == nil || config.files == nil {
		return "", errors.New("No files loaded.")
	}

	for e := config.files.Front(); e != nil; e = e.Next() {
		file := e.Value.(string)
		dir := path.Dir(file)
		targetFile := path.Join(dir, relativeFile)
		_, err := os.Stat(targetFile)
		if err == nil {
			return targetFile, err
		}
	}

	return "", errors.New("File not found.")
}
