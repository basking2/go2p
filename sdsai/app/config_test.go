package app

import (
	"strings"
	"testing"
)

func TestConfig(t *testing.T) {
	t.Parallel()

	c := NewConfig()

	err := c.LoadString(`# This is a comment. Below is an empty line.
        a=b

        this is wrong`)

	if err == nil {
		t.Fail()
	}

	if err != nil && strings.Compare("Invalid configuration on line 4.", err.Error()) != 0 {
		t.Errorf("Expected line 3. Got %s.", err.Error())
	}
}
