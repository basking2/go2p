package app

import (
	"strconv"
	"strings"
)

/*
  Wraps a string as a version that can be comapred in a sane way.
  The string is split on "." characters and is compared from
  left-to-right with the left-most token being the most significant.
  Tokens are converted into numbers, if possible, before comparison.
*/
type Version string

type versionElement struct {
	num      int64
	tok      string
	numValid bool
}

func (v Version) toVersionElements() []versionElement {
	tokens := strings.Split(string(v), ".")
	elements := make([]versionElement, len(tokens))

	for idx, tok := range tokens {
		num, err := strconv.ParseInt(tok, 0, 64)
		ele := &elements[idx]
		ele.tok = tok
		if err == nil {
			ele.num = num
			ele.numValid = true
		} else {
			ele.num = 0
			ele.numValid = false
		}
	}

	return elements
}

// Return true if this is less than that.
func Compare(this Version, that Version) int {

	elements1 := this.toVersionElements()
	elements2 := that.toVersionElements()

	var ilimit int
	if len(elements1) < len(elements2) {
		ilimit = len(elements1)
	} else {
		ilimit = len(elements2)
	}

	// Check that the common prefixes are the same.
	for i := 0; i < ilimit; i++ {
		el1 := elements1[i]
		el2 := elements2[i]

		if el1.numValid && el2.numValid {
			if el1.num < el2.num {
				return -1
			} else if el1.num > el2.num {
				return 1
			}
		} else {
			switch strings.Compare(el1.tok, el2.tok) {
			case -1:
				return -1
			case 1:
				return 1
			}
		}
	}

	// If both arrays are identical up through the shortest,
	// then the longer is larger.
	if len(elements1) > len(elements2) {
		return 1
	} else if len(elements1) < len(elements2) {
		return -1
	}

	return 0
}

func (this Version) Gt(that Version) bool {
	return Compare(this, that) == 1
}

func (this Version) Lt(that Version) bool {
	return Compare(this, that) == -1
}

func (this Version) Eq(that Version) bool {
	return Compare(this, that) == 0
}
