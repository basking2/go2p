package app

import (
	"testing"
)

func TestVersionLt(t *testing.T) {

	t.Parallel()

	v1 := Version("1.0.2")
	v2 := Version("1.0.3")

	if !v1.Lt(v2) {
		t.Fail()
	}

	if v2.Lt(v1) {
		t.Fail()
	}

	if v1.Lt(v1) {
		t.Fail()
	}

	if v2.Lt(v2) {
		t.Fail()
	}
}

func TestVersionLtShorter(t *testing.T) {

	t.Parallel()

	v1 := Version("1.0")
	v2 := Version("1.0.3")

	if !v1.Lt(v2) {
		t.Fail()
	}

	if v2.Lt(v1) {
		t.Fail()
	}

	if v1.Lt(v1) {
		t.Fail()
	}

	if v2.Lt(v2) {
		t.Fail()
	}
}

func TestVersionEq(t *testing.T) {

	t.Parallel()

	v1 := Version("1.0.3")
	v2 := Version("1.0.3")

	if !v1.Eq(v2) {
		t.Fail()
	}
}
