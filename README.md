## Rules of the Game

This is, generally, how the system works.

1. The system consistes of a P2p object that holds.
   1. A map of IDs to peers.
   2. A map of IDs to connections.
   3. A list of services that handle messages of a given type.
2. Every peer we know about is in the map of IDs to peers.
3. If we have a direct connection to a peer, it is listed in the map
   of IDs to connections using the peer's ID.
   1. This implies that we may only have a single connection to a peer at
      a time.
   2. This implies that every time we receive a message we can know
      which peer it comes from and may update the livelyness of that peer.
   3. When we make a connection there must be a hand-shaking to identify
      the remote (or client) peer.
4. When we recieve a message it has meta data and data.
   1. The meta data has a +to+ id, a +from+ id, a type field and a +hops+ count.
   2. The data is opaque. We know nothing about it.
5. When receiving a message... _Fill this in..._
5. When sending a message... _Fill this in..._



## Peer Model

This describes how we think of peers.

A peer is another instance of this Peer-to-peer service running.
A peer has an id that uniquely identifies it.
You may only have a single direct connection to a peer at a time.
You may be indirectly connected to a peer through a connection to
another peer.

A connection has an id. That connection id is the id of the peer at
the other end of the connection.

Peer records in our system also keep a copy of the last connection 
that peer was observed through. If the peer is directly connected,
that connection id will equal that peer's id. Otherwise the peer
will have another id or an empty string (empty id) if we don't know
that we've ever seen this peer.

## Protocol

On initial connection establishment a peer shall send
a message. This message should have a `from` value set to the
peer's id and any `to` value. The data length may be 0
and the hops may be 0.

This first message is critical for labelling the connection
correctly.

## Config

    p2p.bind=::
    p2p.port=8654
    p2p.certs.file=cert.pem
    p2p.keys.file=privkey.pem
    ui.certs.file=cert.pem
    ui.keys.file=privkey.pem

## URLs


    GET - /txt/message/{id}
    PUT POST - /txt/message/{id}
    GET /txt/peer/id/{id}/addr/{addr}

## Bugs / Features

1. Forwarding messages needs to NOT send the message back over the link that
   message was received on.
